#include <iostream>
#include <string>
#include <stdexcept>
#include "C:/Program Files/PostgreSQL/16/include/libpq-fe.h"

void RecebimentoProd(const std::string& codigoDeBarras) {
    try {
        PGconn *conn = PQconnectdb("dbname=mercado host=localhost user=mercado password=mercado");
        if (PQstatus(conn) == CONNECTION_BAD) {
            throw std::runtime_error("Não foi possível estabelecer conexão com o banco de dados.");
        }

        std::string codbar;
        int qnt;

        std::cout << "Insira o código de barras do produto: ";
        std::cin >> codbar;

        std::cout << "Insira a quantidade adicionada desse produto: ";
        if (!(std::cin >> qnt) || qnt <= 0) {
            throw std::invalid_argument("A quantidade inserida não é válida.");
        }

        std::string consulta = "SELECT * FROM quant_produtos WHERE codigo_de_barra = $1;";
        const char* params[1] = { codbar.c_str() };
        PGresult* res = PQexecParams(conn, consulta.c_str(), 1, nullptr, params, nullptr, nullptr, 0);

        if (PQresultStatus(res) != PGRES_TUPLES_OK) {
            throw std::runtime_error("O recebimento de produtos falhou: " + std::string(PQerrorMessage(conn)));
        }

        if (PQntuples(res) == 0) {
            std::cerr << "Produto com código de barras " << codbar << " não encontrado." << std::endl;
        } else {
            int quantidadeAtual = std::stoi(PQgetvalue(res, 0, 1));
            std::cout << "Produto " << codbar << " tem " << quantidadeAtual << " em estoque." << std::endl;
            quantidadeAtual += qnt;

            consulta = "UPDATE quant_produtos SET quant_estoque = $1 WHERE codigo_de_barra = $2;";
            const char* paramsUpdate[2] = { std::to_string(quantidadeAtual).c_str(), codbar.c_str() };
            res = PQexecParams(conn, consulta.c_str(), 2, nullptr, paramsUpdate, nullptr, nullptr, 0);

            if (PQresultStatus(res) != PGRES_COMMAND_OK) {
                throw std::runtime_error("Falha ao atualizar a quantidade de produtos: " + std::string(PQerrorMessage(conn)));
            }
            std::cout << "Quantidade de produtos atualizada com sucesso." << std::endl;
        }

        PQclear(res);
        PQfinish(conn);
    } catch (const std::exception& e) {
        std::cerr << "Erro: " << e.what() << std::endl;
    }
}

int main() {
    std::string codigoDeBarras;
    std::cout << "Digite o código de barras do produto: ";
    std::cin >> codigoDeBarras;
    RecebimentoProd(codigoDeBarras);
    return 0;
}