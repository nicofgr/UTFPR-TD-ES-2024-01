#include <iostream>
#include "libpq-fe.h"  // Biblioteca para acessar o banco de dados

using namespace std;

void ConsultaEstoque(const string& codbar){

    PGconn *conn;  // Conexao com a db

    conn = PQconnectdb("dbname=mercado host=localhost user=mercado password=mercado");

    if (PQstatus(conn) == CONNECTION_BAD) {  // Testando conexao
        cout << "Nao foi possivel estabelecer conexao com o banco de dados." << endl;
        exit(0);
    }

    cout << "insira o codigo de barras do produto" << endl; // Recebe o código de barras
    cin >> codbar;

    string consulta = "SELECT * FROM quant_produtos WHERE codigo_de_barra = '" + codbar + "';"; //Concatena o código de barras em formato de string com o comando SELECT do banco de dados
    PGresult* res = PQexec(conn, consulta.c_str()); //realiza a consulta

        if (PQresultStatus(res) != PGRES_TUPLES_OK) { //verifica erro
            cerr << "A consulta falhou: " << PQerrorMessage(conn) << endl;
        }
        else {
        int num_rows = PQntuples(res);

        for (int i = 0; i < num_rows; i++) {
            cout << "Quantidade em estoque: " << PQgetvalue(res, i, 1) << endl; //imprime quantidade em estoque
        }
    }

    PQclear(res);
    PQfinish(conn);
}






