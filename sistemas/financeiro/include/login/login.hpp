#ifndef LOGIN_H_INCLUDED
#define LOGIN_H_INCLUDED

#include <gtkmm.h>
#include "./financeiroWindow.hpp"

class Login: public Gtk::Window
{
public:
  Login();

protected:
  void on_confirm_clicked();
  void on_quit_clicked();
  void financeiro_close();


  FinanceiroW financeiro;
  Gtk::Box h_box_button;
  Gtk::Box v_box, v_box_name, v_box_password;
  Gtk::Entry entry_name;
  Gtk::Entry entry_password;
  Gtk::Button confirm_button;
  Gtk::Button quit_button;
  Gtk::Label mensage_error, name, password;


};

#endif