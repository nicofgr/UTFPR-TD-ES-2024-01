#ifndef MOSTRA_CADASTRA_FORNECEDOR_H_INCLUDED
#define MOSTRA_CADASTRA_FORNECEDOR_H_INCLUDED
#include <gtkmm.h>
#include <string>
#include <vector>
#include "./financeiroWidgets.hpp"


class Cadastra_fornecedor: public Gtk::Window {
    public:
        Cadastra_fornecedor();
        void set_db(mercado_dbAPI* db_financeiro);
    protected:

        mercado_dbAPI* db;

        void on_confirm();
        void on_quit();

        Gtk::Button confirm_button;
        Gtk::Button quit_button;
        Gtk::Entry entry_cnpj, entry_name;
        Gtk::Box v_box, h_box_button, v_box_name, v_box_cnpj;
        Gtk::Label label_cnpj, label_name, label_aviso;

    };

#endif