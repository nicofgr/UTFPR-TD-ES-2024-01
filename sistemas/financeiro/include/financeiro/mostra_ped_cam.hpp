#ifndef MOSTRA_PED_CAM_H_INCLUDED
#define MOSTRA_PED_CAM_H_INCLUDED
#include <gtkmm.h>
#include <string>
#include <vector>
#include "./financeiroWidgets.hpp"

class Mostra_produtos_caminho: public Gtk::Window {
public:
    Mostra_produtos_caminho();
    void set_table(std::vector<std::vector<std::string>> tabela_f);
protected:

  
    // Signal handlers:
  void on_button_quit();
  void on_setup_label(const Glib::RefPtr<Gtk::ListItem>& list_item, Gtk::Align halign);
  void on_bind_prod_id(const Glib::RefPtr<Gtk::ListItem>& list_item);
  void on_bind_forn_id(const Glib::RefPtr<Gtk::ListItem>& list_item);
  void on_bind_data(const Glib::RefPtr<Gtk::ListItem>& list_item);
  void on_bind_status(const Glib::RefPtr<Gtk::ListItem>& list_item);
  

    
  // A Gio::ListStore item.
  class ModelColumns : public Glib::Object
  {
  public:
    Glib::ustring m_col_prod_id;
    Glib::ustring m_col_forn_id;
    Glib::ustring m_col_data;
    Glib::ustring m_col_status;

    static Glib::RefPtr<ModelColumns> create(const Glib::ustring& produto_id,
      const Glib::ustring& fornecedor_id, const Glib::ustring& data, const Glib::ustring& status)
    {
      return Glib::make_refptr_for_instance<ModelColumns>(
        new ModelColumns(produto_id, fornecedor_id, data, status));
    }

  protected:
    ModelColumns(const Glib::ustring& produto_id,
      const Glib::ustring& fornecedor_id, const Glib::ustring& data, const Glib::ustring& status)
    : m_col_prod_id(produto_id), m_col_forn_id(fornecedor_id), m_col_data(data), m_col_status(status)
    {}
  }; // ModelColumns

  std::vector<std::vector<std::string>> tabela;

  // Child widgets:
  Gtk::Box m_VBox;
  Gtk::ScrolledWindow m_ScrolledWindow;
  Gtk::ColumnView m_ColumnView;
  Gtk::Box m_ButtonBox;
  Gtk::Button m_Button_Quit;

  Glib::RefPtr<Gio::ListStore<ModelColumns>> m_ListStore;

};


#endif