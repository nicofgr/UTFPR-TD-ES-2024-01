#ifndef MOSTRA_CADASTRA_PRODUTO_H_INCLUDED
#define MOSTRA_CADASTRA_PRODUTO_H_INCLUDED
#include <gtkmm.h>
#include <string>
#include <vector>
#include "./financeiroWidgets.hpp"


class Cadastra_produto: public Gtk::Window {
    public:
        Cadastra_produto();
        void set_db(mercado_dbAPI* db_financeiro);
    protected:

        mercado_dbAPI* db;

        void on_confirm();
        void on_quit();

        Gtk::Button confirm_button;
        Gtk::Button quit_button;
        Gtk::Entry entry_name;
        Gtk::Box v_box, h_box_button;
        Gtk::Label label_name, label_aviso;

    };

#endif