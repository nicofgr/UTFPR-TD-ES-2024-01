#ifndef MOSTRA_PROD_PED_H_INCLUDED
#define MOSTRA_PROD_PED_H_INCLUDED
#include <gtkmm.h>
#include <string>
#include <vector>
#include "./financeiroWidgets.hpp"



class Mostra_produtos_pedido_lista: public Gtk::Window {
public:
    Mostra_produtos_pedido_lista();
    void set_table(std::vector<std::vector<std::string>> tabela_f, std::string pedido_f);
protected:

  
    // Signal handlers:
  void on_button_quit();
  void on_setup_label(const Glib::RefPtr<Gtk::ListItem>& list_item, Gtk::Align halign);
  void on_bind_produtos(const Glib::RefPtr<Gtk::ListItem>& list_item);
  void on_bind_quant(const Glib::RefPtr<Gtk::ListItem>& list_item);
  void on_bind_valor(const Glib::RefPtr<Gtk::ListItem>& list_item);
  

    
  // A Gio::ListStore item.
  class ModelColumns : public Glib::Object
  {
  public:
    Glib::ustring m_col_produtos;
    Glib::ustring m_col_quant;
    Glib::ustring m_col_valor;

    static Glib::RefPtr<ModelColumns> create(const Glib::ustring& produtos,
      const Glib::ustring& quantidade, const Glib::ustring& valor)
    {
      return Glib::make_refptr_for_instance<ModelColumns>(
        new ModelColumns(produtos, quantidade, valor));
    }

  protected:
    ModelColumns(const Glib::ustring& produtos,
      const Glib::ustring& quantidade, const Glib::ustring& valor)
    : m_col_produtos(produtos), m_col_quant(quantidade), m_col_valor(valor)
    {}
  }; // ModelColumns

  std::vector<std::vector<std::string>> tabela;
  std::string pedido;

  // Child widgets:
  Gtk::Box m_VBox;
  Gtk::ScrolledWindow m_ScrolledWindow;
  Gtk::ColumnView m_ColumnView;
  Gtk::Box m_ButtonBox;
  Gtk::Button m_Button_Quit;

  Glib::RefPtr<Gio::ListStore<ModelColumns>> m_ListStore;

};


class Mostra_produtos_pedido: public Gtk::Window {
    public:
        Mostra_produtos_pedido();
        void set_db(mercado_dbAPI* db_financeiro);
    protected:

        mercado_dbAPI* db;

        void on_confirm();
        void on_quit();
        void on_close();
        

        Mostra_produtos_pedido_lista lista;
        Gtk::Button confirm;
        Gtk::Button quit;
        Glib::RefPtr<Gtk::Adjustment> adjustment_pedido;
        Gtk::SpinButton spinButton_pedido;
        Gtk::Box v_box, h_box_button;
        Gtk::Label label_pedido;

    };

#endif