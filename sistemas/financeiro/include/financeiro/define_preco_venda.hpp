#ifndef MOSTRA_DEFINE_PRECO_VENDA_H_INCLUDED
#define MOSTRA_DEFINE_PRECO_VENDA_H_INCLUDED
#include <gtkmm.h>
#include <string>
#include <vector>
#include "./financeiroWidgets.hpp"


class Define_preco_venda: public Gtk::Window {
    public:
        Define_preco_venda();
        void set_db(mercado_dbAPI* db_financeiro);
    protected:

        mercado_dbAPI* db;

        void on_confirm();
        void on_quit();

        Gtk::Button confirm_button;
        Gtk::Button quit_button;
        Glib::RefPtr<Gtk::Adjustment> adjustment_id, adjustment_value, adjustment_est, adjustment_gon;
        Gtk::SpinButton spinButton_id, spinButton_value, spinButton_est, spinButton_gon;
        Gtk::Box v_box, h_box_button, v_box_id, v_box_value, v_box_est, v_box_gon;
        Gtk::Label label_id, label_value, label_est, label_gon, label_aviso;

    };

#endif