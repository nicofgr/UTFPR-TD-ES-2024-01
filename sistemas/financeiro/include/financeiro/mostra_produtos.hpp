#ifndef MOSTRA_PRODUTOS_H_INCLUDED
#define MOSTRA_PRODUTOS_H_INCLUDED
#include <gtkmm.h>
#include <string>
#include <vector>
#include "./financeiroWidgets.hpp"

class Mostra_produtos: public Gtk::Window {
public:
    Mostra_produtos();
    void set_table(std::vector<std::vector<std::string>> tabela_f);
protected:

  
    // Signal handlers:
  void on_button_quit();
  void on_setup_label(const Glib::RefPtr<Gtk::ListItem>& list_item, Gtk::Align halign);
  void on_bind_prod_id(const Glib::RefPtr<Gtk::ListItem>& list_item);
  void on_bind_nome(const Glib::RefPtr<Gtk::ListItem>& list_item);
  void on_bind_cod(const Glib::RefPtr<Gtk::ListItem>& list_item);
  void on_bind_gon(const Glib::RefPtr<Gtk::ListItem>& list_item);
  void on_bind_est(const Glib::RefPtr<Gtk::ListItem>& list_item);
  

    
  // A Gio::ListStore item.
  class ModelColumns : public Glib::Object
  {
  public:
    Glib::ustring m_col_prod_id;
    Glib::ustring m_col_nome;
    Glib::ustring m_col_cod;
    Glib::ustring m_col_gon;
    Glib::ustring m_col_est;

    static Glib::RefPtr<ModelColumns> create(const Glib::ustring& produto_id, const Glib::ustring& nome, const Glib::ustring& cod, 
    const Glib::ustring& gondola, const Glib::ustring& estoque)
    {
      return Glib::make_refptr_for_instance<ModelColumns>(
        new ModelColumns(produto_id, nome, cod, gondola, estoque));
    }

  protected:
    ModelColumns(const Glib::ustring& produto_id, const Glib::ustring& nome, const Glib::ustring& cod, 
    const Glib::ustring& gondola, const Glib::ustring& estoque)
    : m_col_prod_id(produto_id), m_col_nome(nome), m_col_cod(cod), m_col_gon(gondola), m_col_est(estoque)
    {}
  }; // ModelColumns

  std::vector<std::vector<std::string>> tabela;

  // Child widgets:
  Gtk::Box m_VBox;
  Gtk::ScrolledWindow m_ScrolledWindow;
  Gtk::ColumnView m_ColumnView;
  Gtk::Box m_ButtonBox;
  Gtk::Button m_Button_Quit;

  Glib::RefPtr<Gio::ListStore<ModelColumns>> m_ListStore;

};


#endif