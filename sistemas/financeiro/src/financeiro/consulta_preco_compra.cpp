#include "../../include/financeiro/consulta_preco_compra.hpp"
#include <string>

Consulta_preco_compra::Consulta_preco_compra()
:
    confirm("Confirmar"),
    quit("Sair"),
    v_box(Gtk::Orientation::VERTICAL),
    label_pedido("Numero do pedido: ", Gtk::Align::START),
    adjustment_pedido( Gtk::Adjustment::create(1.0, 1.0, 1000.0, 1.0, 5.0, 0.0) ),
    spinButton_pedido(adjustment_pedido)
{
    
    set_default_size(200, 100);
    set_title("Consulta valor de compra");

    set_child(v_box);

    

    label_pedido.set_expand();
    v_box.append(label_pedido);
    v_box.append(spinButton_pedido);
    spinButton_pedido.set_wrap();
    spinButton_pedido.set_expand();

    v_box.append(label_pedido);
    v_box.append(label_valor);

    v_box.set_spacing(5);

    quit.signal_clicked().connect( sigc::mem_fun(*this,
              &Consulta_preco_compra::on_quit) );
    h_box_button.append(quit);
    quit.set_expand();

    confirm.signal_clicked().connect( sigc::mem_fun(*this,
              &Consulta_preco_compra::on_confirm) );
    h_box_button.set_spacing(5);
    h_box_button.append(confirm);
    confirm.set_expand();


    v_box.set_spacing(10);
    v_box.append(h_box_button);

}

void Consulta_preco_compra::on_confirm()
{   
   
    std::string nome = db->get_nome_produto(spinButton_pedido.get_value_as_int());
    std::vector<std::string> valor = db->get_valores_compra(spinButton_pedido.get_value_as_int());

    
    Glib::RefPtr<Gtk::Application> app = get_application();
	std::vector<Window*> windows = app->get_windows();
	
    app->add_window(lista);

    lista.set_table(valor, nome);

    lista.signal_close_request().connect([this]() -> bool {
        this->lista.hide();
        return true; 
    }, false);
    
    //lista.signal_hide().connect(sigc::mem_fun(*this, &Mostra_produtos_pedido::on_close));
    lista.show();
    
}

void Consulta_preco_compra::on_quit()
{
    set_visible(false);
}

void Consulta_preco_compra::on_close()
{
   
}

void Consulta_preco_compra::set_db(mercado_dbAPI* db_financeiro)
{
    db = db_financeiro;
    
}

Lista_valores::Lista_valores()
: 
    m_VBox(Gtk::Orientation::VERTICAL),
    m_Button_Quit("Sair")
{
  set_title(pedido);
  set_default_size(250, 250);

  m_VBox.set_margin(5);
  set_child(m_VBox);

  // Add the ColumnView, inside a ScrolledWindow, with the button underneath:
  m_ScrolledWindow.set_child(m_ColumnView);

  // Only show the scrollbars when they are necessary:
  m_ScrolledWindow.set_policy(Gtk::PolicyType::AUTOMATIC, Gtk::PolicyType::AUTOMATIC);
  m_ScrolledWindow.set_expand();

  m_VBox.append(m_ScrolledWindow);
  m_VBox.append(m_ButtonBox);

  m_ButtonBox.append(m_Button_Quit);
  m_ButtonBox.set_margin(5);
  m_Button_Quit.set_hexpand(true);
  m_Button_Quit.set_halign(Gtk::Align::END);
  m_Button_Quit.signal_clicked().connect(
    sigc::mem_fun(*this, &Lista_valores::on_button_quit));

  // Create the List model:
  


  // Number column
  auto factory = Gtk::SignalListItemFactory::create();
  factory->signal_setup().connect(sigc::bind(sigc::mem_fun(*this,
    &Lista_valores::on_setup_label), Gtk::Align::START));
  factory->signal_bind().connect(
    sigc::mem_fun(*this, &Lista_valores::on_bind_valor));
  auto column = Gtk::ColumnViewColumn::create("Valores", factory);
  m_ColumnView.append_column(column);

}

void Lista_valores::set_table(std::vector<std::string> tabela_f, std::string pedido_f){

    tabela = tabela_f;
    pedido = pedido_f;
    m_ListStore = Gio::ListStore<ModelColumns>::create();
  
    int row_max = tabela.size();
    for (int row=0; row<row_max; row++) {
            m_ListStore->append(ModelColumns::create(tabela[row]));
        }

    // Set list model and selection model.
    auto selection_model = Gtk::SingleSelection::create(m_ListStore);
    selection_model->set_autoselect(false);
    selection_model->set_can_unselect(true);
    m_ColumnView.set_model(selection_model);
    m_ColumnView.add_css_class("data-table"); // high density table

    // Make the columns reorderable.
    // This is not necessary, but it's nice to show the feature.
    m_ColumnView.set_reorderable(true);
    set_title(pedido);
}

void Lista_valores::on_button_quit()
{
  set_visible(false);
}

void Lista_valores::on_setup_label(
  const Glib::RefPtr<Gtk::ListItem>& list_item, Gtk::Align halign)
{
  list_item->set_child(*Gtk::make_managed<Gtk::Label>("", halign));
}


void Lista_valores::on_bind_valor(const Glib::RefPtr<Gtk::ListItem>& list_item)
{
  auto col = std::dynamic_pointer_cast<ModelColumns>(list_item->get_item());
  if (!col)
    return;
  auto label = dynamic_cast<Gtk::Label*>(list_item->get_child());
  if (!label)
    return;
  label->set_text(col->m_col_valor);
}