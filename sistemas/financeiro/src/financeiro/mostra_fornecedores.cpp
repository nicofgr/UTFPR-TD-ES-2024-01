#include "../../include/financeiro/mostra_fornecedores.hpp"
Mostra_forncedores::Mostra_forncedores()
: 
    m_VBox(Gtk::Orientation::VERTICAL),
    m_Button_Quit("Sair")
{
  set_title("Fornecedores");
  set_default_size(300, 250);

  m_VBox.set_margin(5);
  set_child(m_VBox);

  // Add the ColumnView, inside a ScrolledWindow, with the button underneath:
  m_ScrolledWindow.set_child(m_ColumnView);

  // Only show the scrollbars when they are necessary:
  m_ScrolledWindow.set_policy(Gtk::PolicyType::AUTOMATIC, Gtk::PolicyType::AUTOMATIC);
  m_ScrolledWindow.set_expand();

  m_VBox.append(m_ScrolledWindow);
  m_VBox.append(m_ButtonBox);

  m_ButtonBox.append(m_Button_Quit);
  m_ButtonBox.set_margin(5);
  m_Button_Quit.set_hexpand(true);
  m_Button_Quit.set_halign(Gtk::Align::END);
  m_Button_Quit.signal_clicked().connect(
    sigc::mem_fun(*this, &Mostra_forncedores::on_button_quit));

  // Create the List model:
  

  // Add the ColumnView's columns:

  // Name column
  auto factory = Gtk::SignalListItemFactory::create();
  factory->signal_setup().connect(sigc::bind(sigc::mem_fun(*this,
    &Mostra_forncedores::on_setup_label), Gtk::Align::START));
  factory->signal_bind().connect(
    sigc::mem_fun(*this, &Mostra_forncedores::on_bind_forn_id));
  auto column = Gtk::ColumnViewColumn::create("ID Forncedor", factory);
  m_ColumnView.append_column(column);

  // Number column
  factory = Gtk::SignalListItemFactory::create();
  factory->signal_setup().connect(sigc::bind(sigc::mem_fun(*this,
    &Mostra_forncedores::on_setup_label), Gtk::Align::START));
  factory->signal_bind().connect(
    sigc::mem_fun(*this, &Mostra_forncedores::on_bind_nome));
  column = Gtk::ColumnViewColumn::create("Fornecedor", factory);
  m_ColumnView.append_column(column);

  factory = Gtk::SignalListItemFactory::create();
  factory->signal_setup().connect(sigc::bind(sigc::mem_fun(*this,
    &Mostra_forncedores::on_setup_label), Gtk::Align::START));
  factory->signal_bind().connect(
    sigc::mem_fun(*this, &Mostra_forncedores::on_bind_cnpj));
  column = Gtk::ColumnViewColumn::create("CNPJ", factory);
  m_ColumnView.append_column(column);

}

void Mostra_forncedores::set_table(std::vector<std::vector<std::string>> tabela_f){

    tabela = tabela_f;
    m_ListStore = Gio::ListStore<ModelColumns>::create();
  
    int row_max = tabela.size();
    for (int row=0; row<row_max; row++) {
            m_ListStore->append(ModelColumns::create(tabela[row][0], tabela[row][1], tabela[row][2]));
        }

    // Set list model and selection model.
    auto selection_model = Gtk::SingleSelection::create(m_ListStore);
    selection_model->set_autoselect(false);
    selection_model->set_can_unselect(true);
    m_ColumnView.set_model(selection_model);
    m_ColumnView.add_css_class("data-table"); // high density table

    // Make the columns reorderable.
    // This is not necessary, but it's nice to show the feature.
    m_ColumnView.set_reorderable(true);
}

void Mostra_forncedores::on_button_quit()
{
  set_visible(false);
}

void Mostra_forncedores::on_setup_label(
  const Glib::RefPtr<Gtk::ListItem>& list_item, Gtk::Align halign)
{
  list_item->set_child(*Gtk::make_managed<Gtk::Label>("", halign));
}


void Mostra_forncedores::on_bind_forn_id(const Glib::RefPtr<Gtk::ListItem>& list_item)
{
  auto col = std::dynamic_pointer_cast<ModelColumns>(list_item->get_item());
  if (!col)
    return;
  auto label = dynamic_cast<Gtk::Label*>(list_item->get_child());
  if (!label)
    return;
  label->set_text(col->m_col_forn_id);
}

void Mostra_forncedores::on_bind_nome(const Glib::RefPtr<Gtk::ListItem>& list_item)
{
  auto col = std::dynamic_pointer_cast<ModelColumns>(list_item->get_item());
  if (!col)
    return;
  auto label = dynamic_cast<Gtk::Label*>(list_item->get_child());
  if (!label)
    return;
  label->set_text(col->m_col_nome);
}

void Mostra_forncedores::on_bind_cnpj(const Glib::RefPtr<Gtk::ListItem>& list_item)
{
  auto col = std::dynamic_pointer_cast<ModelColumns>(list_item->get_item());
  if (!col)
    return;
  auto label = dynamic_cast<Gtk::Label*>(list_item->get_child());
  if (!label)
    return;
  label->set_text(col->m_col_cnpj);
}