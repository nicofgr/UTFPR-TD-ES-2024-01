#include "../../include/financeiro/fazer_pedido.hpp"
#include "fazer_pedido.hpp"
#include <iomanip>

Fazer_pedido::Fazer_pedido()
:
    confirm("Iniciar pedido"),
    quit("Sair"),
    v_box(Gtk::Orientation::VERTICAL),
    label_pedido("Numero do forncedor: ", Gtk::Align::START),
    label_erro("", Gtk::Align::START),
    adjustment_pedido( Gtk::Adjustment::create(1.0, 1.0, 1000.0, 1.0, 5.0, 0.0) ),
    spinButton_pedido(adjustment_pedido),
    pedido_status(0)
{
    
    set_default_size(200, 100);
    set_title("Fazer pedido");

    set_child(v_box);

    

    label_pedido.set_expand();
    v_box.append(label_pedido);
    v_box.append(spinButton_pedido);
    spinButton_pedido.set_wrap();
    spinButton_pedido.set_expand();

    v_box.set_spacing(5);
    v_box.append(label_erro);

    quit.signal_clicked().connect( sigc::mem_fun(*this,
              &Fazer_pedido::on_quit) );
    h_box_button.append(quit);
    quit.set_expand();

    confirm.signal_clicked().connect( sigc::mem_fun(*this,
              &Fazer_pedido::on_confirm) );
    h_box_button.set_spacing(5);
    h_box_button.append(confirm);
    confirm.set_expand();


    v_box.set_spacing(10);
    v_box.append(h_box_button);

    lista.signal_hide().connect(sigc::mem_fun(*this, &Fazer_pedido::on_close));

}

void Fazer_pedido::on_confirm()
{   
    if(pedido_status) return;
    
    int pedido = spinButton_pedido.get_value_as_int();
    
    
    
    int pedido_id = db->abre_pedido(pedido);

    
    //db.mercado_dbAPI_connect();
    if(pedido_id == -1 ){
        label_erro.set_text("ID fornecedor não existe");
        return;
    }
    label_erro.set_text("");
    pedido_status = 1;
    Glib::RefPtr<Gtk::Application> app = get_application();
	  std::vector<Window*> windows = app->get_windows();
	
    app->add_window(lista);

    lista.set_db(db, pedido_id);

    lista.signal_close_request().connect([this]() -> bool {
        //this->lista.hide();
        return true; 
    }, false);
    
    
    lista.show();
}

void Fazer_pedido::on_quit()
{
    set_visible(false);
}

void Fazer_pedido::on_close()
{
   pedido_status = 0;
}

void Fazer_pedido::set_db(mercado_dbAPI* db_financeiro)
{
    db = db_financeiro;
    
}

Lista_pedidos::Lista_pedidos()
: 
    m_VBox(Gtk::Orientation::VERTICAL),
    m_Button_Quit("Fechar pedido"),
    m_Button_Adicionar("Adicionar ao pedido"),
    pedido_status(0),
    label_erro("", Gtk::Align::START)
{
  set_title("Lista de pedidos");
  set_default_size(300, 250);

  m_VBox.set_margin(5);
  set_child(m_VBox);

  // Add the ColumnView, inside a ScrolledWindow, with the button underneath:
  m_ScrolledWindow.set_child(m_ColumnView);

  // Only show the scrollbars when they are necessary:
  m_ScrolledWindow.set_policy(Gtk::PolicyType::AUTOMATIC, Gtk::PolicyType::AUTOMATIC);
  m_ScrolledWindow.set_expand();

  m_VBox.append(m_ScrolledWindow);
  m_VBox.append(label_erro);
  m_VBox.append(m_ButtonBox);

  m_ButtonBox.append(m_Button_Quit);
  m_ButtonBox.set_margin(5);
  m_Button_Quit.set_hexpand(true);
  m_Button_Quit.set_halign(Gtk::Align::START);
  m_Button_Quit.signal_clicked().connect(
    sigc::mem_fun(*this, &Lista_pedidos::on_button_quit));

    m_ButtonBox.append(m_Button_Adicionar);
  m_ButtonBox.set_margin(5);
  m_Button_Adicionar.set_hexpand(true);
  m_Button_Adicionar.set_halign(Gtk::Align::END);
  m_Button_Adicionar.signal_clicked().connect(
    sigc::mem_fun(*this, &Lista_pedidos::on_button_adicionar));

  // Create the List model:
  

  // Add the ColumnView's columns:

  // Id column
  auto factory = Gtk::SignalListItemFactory::create();
  factory->signal_setup().connect(sigc::bind(sigc::mem_fun(*this,
    &Lista_pedidos::on_setup_label), Gtk::Align::START));
  factory->signal_bind().connect(
    sigc::mem_fun(*this, &Lista_pedidos::on_bind_produtos));
  auto column = Gtk::ColumnViewColumn::create("Produto", factory);
  m_ColumnView.append_column(column);

  // Name column
  factory = Gtk::SignalListItemFactory::create();
  factory->signal_setup().connect(sigc::bind(sigc::mem_fun(*this,
    &Lista_pedidos::on_setup_label), Gtk::Align::START));
  factory->signal_bind().connect(
    sigc::mem_fun(*this, &Lista_pedidos::on_bind_quant));
  column = Gtk::ColumnViewColumn::create("Quantidade", factory);
  m_ColumnView.append_column(column);

  // Number column
  factory = Gtk::SignalListItemFactory::create();
  factory->signal_setup().connect(sigc::bind(sigc::mem_fun(*this,
    &Lista_pedidos::on_setup_label), Gtk::Align::START));
  factory->signal_bind().connect(
    sigc::mem_fun(*this, &Lista_pedidos::on_bind_valor));
  column = Gtk::ColumnViewColumn::create("Valor", factory);
  m_ColumnView.append_column(column);

  adiciona.signal_hide().connect(sigc::mem_fun(*this, &Lista_pedidos::on_close_adicionar));

  fecha_pedido.signal_hide().connect(sigc::mem_fun(*this, &Lista_pedidos::on_close_fechar));

}

void Lista_pedidos::set_db(mercado_dbAPI* db_financeiro, int pedido_f)
{
    pedido = pedido_f;
    db = db_financeiro;

    label_erro.set_text("");

    m_ListStore = Gio::ListStore<ModelColumns>::create();
  
    auto selection_model = Gtk::SingleSelection::create(m_ListStore);
    selection_model->set_autoselect(false);
    selection_model->set_can_unselect(true);
    m_ColumnView.set_model(selection_model);
    m_ColumnView.add_css_class("data-table");

    m_ColumnView.set_reorderable(true);
    
}


void Lista_pedidos::on_button_quit()
{
    if(pedido_status) return;
    pedido_status = 1;
    Glib::RefPtr<Gtk::Application> app = get_application();
	  std::vector<Window*> windows = app->get_windows();
	
    app->add_window(fecha_pedido);

    fecha_pedido.set_db(db, pedido);

    fecha_pedido.signal_close_request().connect([this]() -> bool {
        //this->fecha_pedido.hide();
        return true; 
    }, false);
    
    
    fecha_pedido.show();
}

void Lista_pedidos::on_close_fechar()
{
    pedido_status = 0;
    set_visible(false);
}

void Lista_pedidos::on_button_adicionar()
{
    if(pedido_status) return;
    pedido_status = 1;

    Glib::RefPtr<Gtk::Application> app = get_application();
        std::vector<Window*> windows = app->get_windows();

    app->add_window(adiciona);

    adiciona.set_db(db, pedido);
    adiciona.signal_close_request().connect([this]() -> bool {
        //this->adiciona.hide();
        return true; 
    }, false);
    
    adiciona.show();
}

void Lista_pedidos::on_close_adicionar()
{
    pedido_status = 0;
    if(adiciona.get_res() == 0){
        label_erro.set_text("Nao foi possivel adicionar o produto ao pedido");
        return;
    } 
    label_erro.set_text("");
    m_ListStore->append(ModelColumns::create(std::to_string(adiciona.get_id()), std::to_string(adiciona.get_quant()), "$"+Glib::ustring::sprintf("%.2f", adiciona.get_valor())));
}

void Lista_pedidos::on_setup_label(
  const Glib::RefPtr<Gtk::ListItem>& list_item, Gtk::Align halign)
{
  list_item->set_child(*Gtk::make_managed<Gtk::Label>("", halign));
}


void Lista_pedidos::on_bind_produtos(const Glib::RefPtr<Gtk::ListItem>& list_item)
{
  auto col = std::dynamic_pointer_cast<ModelColumns>(list_item->get_item());
  if (!col)
    return;
  auto label = dynamic_cast<Gtk::Label*>(list_item->get_child());
  if (!label)
    return;
  label->set_text(col->m_col_produtos);
}

void Lista_pedidos::on_bind_quant(const Glib::RefPtr<Gtk::ListItem>& list_item)
{
  auto col = std::dynamic_pointer_cast<ModelColumns>(list_item->get_item());
  if (!col)
    return;
  auto label = dynamic_cast<Gtk::Label*>(list_item->get_child());
  if (!label)
    return;
  label->set_text(col->m_col_quant);
}

void Lista_pedidos::on_bind_valor(const Glib::RefPtr<Gtk::ListItem>& list_item)
{
  auto col = std::dynamic_pointer_cast<ModelColumns>(list_item->get_item());
  if (!col)
    return;
  auto label = dynamic_cast<Gtk::Label*>(list_item->get_child());
  if (!label)
    return;
  label->set_text(col->m_col_valor);
}

Adicionar::Adicionar()
:
    confirm("Adicionar"),
    v_box(Gtk::Orientation::VERTICAL),
    v_box_id(Gtk::Orientation::VERTICAL), 
    v_box_quant(Gtk::Orientation::VERTICAL), 
    v_box_valor(Gtk::Orientation::VERTICAL),
    label_id("ID produto: ", Gtk::Align::START),
    label_quant("Quantidade: ", Gtk::Align::START),
    label_valor("Valor: ", Gtk::Align::START),
    m_adjustment_id(Gtk::Adjustment::create(1.0, 1.0, 1000.0, 1.0, 5.0, 0.0)),
    m_SpinButton_id(m_adjustment_id),
    m_adjustment_quant(Gtk::Adjustment::create(1.0, 1.0, 1000.0, 1.0, 5.0, 0.0)),
    m_SpinButton_quant(m_adjustment_quant),
    m_adjustment_valor(Gtk::Adjustment::create(1.0, 0.0, 10000.0, 0.5, 100.0, 0.0)),
    m_SpinButton_valor(m_adjustment_valor)
{
    
    set_default_size(300, 100);
    set_title("Fazer pedido");

    set_child(v_box);

    v_box.append(h_box);

    h_box.append(v_box_id);
    h_box.append(v_box_quant);
    h_box.append(v_box_valor);

    h_box.set_spacing(5);

    label_id.set_expand();
    v_box_id.append(label_id);
    v_box_id.append(m_SpinButton_id);
    m_SpinButton_id.set_wrap();
    m_SpinButton_id.set_expand();

    v_box_id.set_spacing(5);

    label_quant.set_expand();
    v_box_quant.append(label_quant);
    v_box_quant.append(m_SpinButton_quant);
    m_SpinButton_quant.set_wrap();
    m_SpinButton_quant.set_expand();

    v_box_quant.set_spacing(5);

    label_valor.set_expand();
    v_box_valor.append(label_valor);
    v_box_valor.append(m_SpinButton_valor);
    m_SpinButton_valor.set_wrap();
    m_SpinButton_valor.set_expand();
    m_SpinButton_valor.set_size_request(100, -1);
    m_SpinButton_valor.set_snap_to_ticks(0);
    m_SpinButton_valor.set_digits(2);
    m_SpinButton_valor.set_numeric(1);

    v_box_valor.set_spacing(5);    


    confirm.signal_clicked().connect( sigc::mem_fun(*this,
              &Adicionar::on_confirm) );
    h_box_button.set_spacing(5);
    h_box_button.append(confirm);
    confirm.set_expand();


    v_box.set_spacing(10);
    v_box.append(h_box_button);

}

void Adicionar::on_confirm()
{   
    id = m_SpinButton_id.get_value_as_int();
    quant = m_SpinButton_quant.get_value_as_int();
    valor =  m_SpinButton_valor.get_value();

    res = db->adiciona_pedido_produto(pedido_id, id, quant, valor);

    hide();

}



void Adicionar::set_db(mercado_dbAPI* db_financeiro, int pedido)
{
    pedido_id = pedido;
    db = db_financeiro;
}
int Adicionar::get_quant()
{
    return quant;
}

int Adicionar::get_id()
{
    return id;
}

int Adicionar::get_res()
{
    return res;
}

double Adicionar::get_valor()
{
    return valor;
}

Fechar_pedido::Fechar_pedido()
:
    confirm("Fechar pedido"),
    v_box(Gtk::Orientation::VERTICAL),
    v_box_day(Gtk::Orientation::VERTICAL), 
    v_box_month(Gtk::Orientation::VERTICAL), 
    v_box_year(Gtk::Orientation::VERTICAL),
    label_day("Dia: ", Gtk::Align::START),
    label_month("Mes: ", Gtk::Align::START),
    label_year("Ano: ", Gtk::Align::START),
    m_adjustment_day(Gtk::Adjustment::create(1.0, 1.0, 31.0, 1.0, 5.0, 0.0)),
    m_SpinButton_day(m_adjustment_day),
    m_adjustment_month(Gtk::Adjustment::create(1.0, 1.0, 12.0, 1.0, 5.0, 0.0)),
    m_SpinButton_month(m_adjustment_month),
    m_adjustment_year(Gtk::Adjustment::create(2024.0, 0.0, 2200.0, 1.0, 100.0, 0.0)),
    m_SpinButton_year(m_adjustment_year)
{
    
    set_default_size(200, 100);
    set_title("Fazer pedido");

    set_child(v_box);

    v_box.append(h_box);

    h_box.append(v_box_day);
    h_box.append(v_box_month);
    h_box.append(v_box_year);

    h_box.set_spacing(5);

    label_day.set_expand();
    v_box_day.append(label_day);
    v_box_day.append(m_SpinButton_day);
    m_SpinButton_day.set_wrap();
    m_SpinButton_day.set_expand();

    v_box_day.set_spacing(5);

    label_month.set_expand();
    v_box_month.append(label_month);
    v_box_month.append(m_SpinButton_month);
    m_SpinButton_month.set_wrap();
    m_SpinButton_month.set_expand();

    v_box_month.set_spacing(5);

    label_year.set_expand();
    v_box_year.append(label_year);
    v_box_year.append(m_SpinButton_year);
    m_SpinButton_year.set_wrap();
    m_SpinButton_year.set_expand();

    v_box_year.set_spacing(5);    


    confirm.signal_clicked().connect( sigc::mem_fun(*this,
              &Fechar_pedido::on_confirm) );
    h_box_button.set_spacing(5);
    h_box_button.append(confirm);
    confirm.set_expand();


    v_box.set_spacing(10);
    v_box.append(h_box_button);

}

void Fechar_pedido::on_confirm()
{   
    std::string day = std::to_string(m_SpinButton_day.get_value_as_int());
    std::string month = std::to_string(m_SpinButton_month.get_value_as_int());
    std::string year =  std::to_string(m_SpinButton_year.get_value_as_int());

    std::string data_prevista = month + "-" + day + "-"+ year ;

    db->fecha_pedido(pedido_id, data_prevista);
    set_visible(false);

}


void Fechar_pedido::set_db(mercado_dbAPI* db_financeiro, int pedido)
{
    pedido_id = pedido;
    db = db_financeiro;
}
