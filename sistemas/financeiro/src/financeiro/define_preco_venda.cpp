#include "../../include/financeiro/define_preco_venda.hpp"

Define_preco_venda::Define_preco_venda()
:
    confirm_button("Confirmar"),
    quit_button("Sair"),
    v_box(Gtk::Orientation::VERTICAL),
    label_id("ID produto: ", Gtk::Align::START),
    label_est("Quantidade minima do produto no estoque: ", Gtk::Align::START),
    label_gon("Quantidade minima do produto na gondola: ", Gtk::Align::START),
    label_value("Valor: ", Gtk::Align::START),
    label_aviso("", Gtk::Align::START),
    adjustment_id(Gtk::Adjustment::create(1.0, 1.0, 1000.0, 1.0, 5.0, 0.0)),
    spinButton_id(adjustment_id),
    adjustment_est(Gtk::Adjustment::create(1.0, 1.0, 1000.0, 1.0, 5.0, 0.0)),
    spinButton_est(adjustment_est),
    adjustment_gon(Gtk::Adjustment::create(1.0, 1.0, 1000.0, 1.0, 5.0, 0.0)),
    spinButton_gon(adjustment_gon),
    adjustment_value(Gtk::Adjustment::create(1.0, 0.0, 10000.0, 0.5, 100.0, 0.0)),
    spinButton_value(adjustment_value)
{
    
    set_default_size(200, 100);
    set_title("Definir preco de venda");

    set_child(v_box);

    label_id.set_expand();
    v_box.append(label_id);
    v_box.append(spinButton_id);
    spinButton_id.set_wrap();
    spinButton_id.set_expand();

    label_value.set_expand();
    v_box.append(label_value);
    v_box.append(spinButton_value);
    spinButton_value.set_wrap();
    spinButton_value.set_expand();
    spinButton_value.set_size_request(100, -1);
    spinButton_value.set_snap_to_ticks(0);
    spinButton_value.set_digits(2);
    spinButton_value.set_numeric(1);

    label_est.set_expand();
    v_box.append(label_est);
    v_box.append(spinButton_est);
    spinButton_est.set_wrap();
    spinButton_est.set_expand();

    label_gon.set_expand();
    v_box.append(label_gon);
    v_box.append(spinButton_gon);
    spinButton_gon.set_wrap();
    spinButton_gon.set_expand();

    v_box.append(label_aviso);


    quit_button.signal_clicked().connect( sigc::mem_fun(*this,
              &Define_preco_venda::on_quit) );
    h_box_button.set_spacing(5);
    h_box_button.append(quit_button);
    quit_button.set_expand();

    confirm_button.signal_clicked().connect( sigc::mem_fun(*this,
              &Define_preco_venda::on_confirm) );
    h_box_button.set_spacing(5);
    h_box_button.append(confirm_button);
    confirm_button.set_expand();


    v_box.set_spacing(10);
    v_box.append(h_box_button);

}

void Define_preco_venda::on_confirm()
{   
   
    db->atualiza_preco_produto(spinButton_id.get_value_as_int(), spinButton_value.get_value());
    db->atualiza_quant_min_produto(spinButton_id.get_value_as_int(), spinButton_est.get_value_as_int(), spinButton_gon.get_value_as_int());
    label_aviso.set_text("Valores de " + db->get_nome_produto(spinButton_id.get_value_as_int()) + " cadastrados com sucesso");
    
}

void Define_preco_venda::on_quit()
{
    set_visible(false);
}


void Define_preco_venda::set_db(mercado_dbAPI* db_financeiro)
{
    db = db_financeiro;
    label_aviso.set_text("");
    
}