#include "../../include/login/login.hpp"

#include <iostream>

Login::Login()
:
    v_box(Gtk::Orientation::VERTICAL),
    v_box_name(Gtk::Orientation::VERTICAL),
    v_box_password(Gtk::Orientation::VERTICAL),
    name("Nome: ", Gtk::Align::START),
    password("Senha: ", Gtk::Align::START),
    confirm_button("Confirmar"),
    quit_button("Sair")
{
    set_default_size(200, 50);
    set_title("Login");

    set_child(v_box);
    /*v_box.append(v_box_name);
    v_box.append(v_box_password);
    

    name.set_expand();
    v_box_name.append(name);
    v_box_name.append(entry_name);
    entry_name.set_expand();

    v_box_name.set_spacing(5);

    password.set_expand();
    v_box_password.append(password);
    v_box_password.append(entry_password);
    entry_password.set_expand();

    v_box_password.set_spacing(5);*/
    v_box.append(h_box_button);

    quit_button.signal_clicked().connect( sigc::mem_fun(*this,
              &Login::on_quit_clicked) );
    h_box_button.append(quit_button);
    quit_button.set_expand();

    confirm_button.signal_clicked().connect( sigc::mem_fun(*this,
              &Login::on_confirm_clicked) );
    h_box_button.set_spacing(5);
    h_box_button.append(confirm_button);
    confirm_button.set_expand();

    v_box.set_spacing(10);


}


void Login::on_confirm_clicked()
{
    
        

    Glib::RefPtr<Gtk::Application> app = get_application();
	//std::vector<Window*> windows = app->get_windows();
	
	app->add_window(financeiro);
	//mainWindow.Create(); //Create just calls show() on the window, tried it as a simple double check, didn't work
	
    
    
    
    //financeiro = new FinanceiroW;
    financeiro.signal_hide().connect(sigc::mem_fun(*this, &Login::financeiro_close));
    financeiro.signal_close_request().connect([this]() -> bool {
        this->financeiro.hide();
        return true; 
    }, false);
    financeiro.show();
}

void Login::financeiro_close(){
    
}


void Login::on_quit_clicked(){
    set_visible(false);
}
