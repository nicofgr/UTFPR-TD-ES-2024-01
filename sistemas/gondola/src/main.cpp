#include <iostream>
#include "../include/caixa.hpp"
#include "../include/estoque.hpp"
#include "../include/preco.hpp"

using namespace std;

int main(){
	int opc = 0;
	FILE *fp;
	trie *arvore = cria_arvore();
	double *dinheiro = (double*)malloc(sizeof(double));

	le_arquivo(fp, arvore, dinheiro);
        
	while(1){
		cout << "*********************************************************" << endl;
        	cout << "*                      Menu Principal                   *" << endl;
        	cout << "*                  1. Consultar estoque                 *" << endl;
		cout << "*                  2. Consultar preco                   *" << endl;
		cout << "*                  3. Iniciar venda                     *" << endl;
        	cout << "*                  4. Sair                              *" << endl;	
        	cout << "*********************************************************" << endl;
		cout << "   Opcao: ";
        	cin >> opc;

		
		switch(opc){
			case 1:
				Estoque estoq;
				estoq.consulta_estoque(arvore);
				break;
			case 2:
				consulta_preco(arvore);
				break;
			case 3:
				operador_de_caixa(arvore, dinheiro);
				break;
			case 4:
				fp = fopen("tabela_modificada.txt","w");
				salva_trie(fp, arvore, dinheiro);

				fclose(fp);
				free(arvore);
				free(dinheiro);

				return 0;	
		}
		cout << endl;
	}
}
