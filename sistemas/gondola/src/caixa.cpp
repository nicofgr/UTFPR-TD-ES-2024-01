#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <iomanip>
#include "../include/caixa.hpp"
#include "../include/lista_venda.hpp"
#include "../include/nota_fiscal.hpp"
#include "../include/informacoes_terminal.hpp"
#include "../include/trie.hpp"
#define MAX 1000

using namespace std;

void operador_de_caixa(trie *arvore, double *dinheiro){
	FILE *fp, *salvo;
	int codigo, quantidade, maior_codigo = 0, remocao;
    	int primeira_compra = 0;
	int confirmacao = 0, escolha = 0;
	char pagamento[MAX], conteudo[MAX];
	double valor_total, valor_pago;
 

	while(1){

        	imprime_comandos(0);
            	scanf("%d", &confirmacao);
		cout << "\n";

           	if(confirmacao == 2) break;
            	else if(confirmacao != 1 && confirmacao != 2) imprime_comandos(1);
            	else{
                	imprime_comandos(2);
               	 	lista *vendas = cria_lista();
                 
			while(1){
                    		cout << "   Escreva o codigo do produto: ";
                    		scanf("%d", &codigo);

				if(codigo == -1) break;
                    		else if(codigo < -3) imprime_comandos(3);
				else if(codigo == -2){
					imprime_comandos(4);
	                                cout << "   Escreva o codigo do produto: ";
	                                scanf("%d", &codigo);
                        		remove_produto_lista(vendas, codigo);
                        		imprime_comandos(5);					
				}
				else if(codigo == -3){
					libera_lista(vendas);
                        		imprime_comandos(6);
					break;
				}
				else
                         		insere_na_tail(vendas, codigo);
			}
			
			//Criando a nota fiscal
               		if(codigo != -3){
				if(primeira_compra == 0){
                			fp = fopen("notas_fiscais.txt", "w");
                   			primeira_compra = 1;
                		}
                		else
                  			fp = fopen("notas_fiscais.txt", "a");
               
               	 		imprime_comandos(7);
				
				valor_total = 0;			
				valor_total = calcula_total_da_venda(vendas, arvore);

				cout << "   Valor Total: " << setprecision(2) << fixed << valor_total << endl;
				
				if(valor_total == 0){
					cout << "\n  ERRO: venda cancelada por erro inesperado" << endl;
					break;
				}
				imprime_comandos(11);
				cin >> escolha;

				while(1){
					if(escolha < 0 || escolha > 3){
						cout << "\n   ERRO: Entrada invalida, escolha uma das opcoes validas" << endl;
						cout << "   Escolha: ";
						cin >> escolha;
					}
					else break;
				}

				if(escolha == 1){
					cout << "\n   Pagamento(R$): ";
					cin >> valor_pago;
					cout << "   Troco(R$): " << setprecision(2) << fixed << valor_pago - valor_total;
				
				}
				else
					valor_pago = valor_total;

				dinheiro[0] += valor_total;
				imprime_no_arquivo(fp, vendas, arvore, escolha, valor_total, valor_pago);
                		libera_lista(vendas);
                		fclose(fp);

				imprime_comandos(12);

			}
		}
	}
	
}
