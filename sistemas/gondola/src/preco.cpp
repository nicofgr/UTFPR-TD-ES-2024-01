#include <iostream>
#include <iomanip>
#include "../include/preco.hpp"

using namespace std;

void consulta_preco(trie *arvore){
	int codigo;
	int *codigo_transformado;
	bloco *buscado;

	do{
        	cout << "\nDigite o codigo de barra (digite 0 para sair): ";
        	cin >> codigo;

        	if (codigo == 0) break;
		
		if(codigo < 0){
			cout << "ERRO: codigo invalido, digite um valor nao negativo";
			continue;
		}

		codigo_transformado = transforma_codigo(codigo);
        	buscado = busca_codigo(arvore, codigo_transformado);
      	 	free(codigo_transformado);

        	if (buscado == NULL) {
            		cout << "Codigo nao encontrado.\n\n";
        	} else {
            		cout << "\n   Produto: " << buscado->descricao << endl;
            		cout <<  "   Preco: R$" << buscado->valor_unitario << "\n\n";
        	}
	} while (true);

}
