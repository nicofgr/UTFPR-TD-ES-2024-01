#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include "../include/nota_fiscal.hpp"

using namespace std;

int quantidade_de_digitos(int codigo){
        int aux_cod = codigo;
        int quantidade = 0;

        while(1){
                if(aux_cod == 0) break;
                quantidade++;
                aux_cod = aux_cod/10;
        }

        return quantidade;
}

void define_espacamento (FILE *fp, char *coluna, char *conteudo, int quant_maior){
        int diferenca, tam;
        int digitos_atual;
        int aux_maior, valor_analisado;
        int i;

	if(strcmp(coluna, "CODIGO") == 0)
		diferenca = strlen(coluna) - 13;
	else
		diferenca = strlen(coluna) - quant_maior;

        if(conteudo == NULL){
                if(diferenca <= 0){
                        for(i = 0; i < ((-1)*diferenca); i++)
                                fprintf(fp, " ");
                }
                fprintf(fp, "      ");
                return;
        }

	tam = strlen(conteudo);
	if(strcmp(coluna, "VAL UNIT") == 0)				//Corrige o caracter '^M' existente em
		tam -= 1;						//char *conteudo (representa new line).

	if(strlen(coluna) < quant_maior){
		i = 0;
		while(1){
			if((tam +  i) >= quant_maior) break;
			fprintf(fp, " ");
			i++;
		}
	}
	else{
		for(i = tam; i < strlen(coluna); i++)
                	fprintf(fp, " ");
	}

        fprintf(fp, "      ");

}


void imprime_no_arquivo (FILE *fp, lista *list, trie *arvore, int escolha, float valor_da_venda, float valor_pago){
        nodo *aux_nodo;
	bloco *produto_encontrado;
	float valor_pagar = 0;
        int maior_codigo = 0, maior_quantidade = 0, maior_descricao = 0, maior_valor_unitario = 0, maior_valor_total = 0;
        int tam;

	aux_nodo = list->head;
        while(1){
                if(aux_nodo == NULL) break;
                if(quantidade_de_digitos(aux_nodo->quantidade) > maior_quantidade)
                        maior_quantidade = quantidade_de_digitos(aux_nodo->quantidade);

		produto_encontrado = busca_codigo(arvore, aux_nodo->codigo);
		if(strlen(produto_encontrado->descricao) > maior_descricao)
			maior_descricao = strlen(produto_encontrado->descricao);
		if(strlen(produto_encontrado->valor_unitario) > maior_valor_unitario)
			maior_valor_unitario = strlen(produto_encontrado->valor_unitario);
                aux_nodo = aux_nodo->prox;
        }

//Criando as colunas
        fprintf(fp, "CODIGO");
        char *string_codigo = (char*)malloc(7*sizeof(char));
	strcpy(string_codigo, "CODIGO");
	define_espacamento(fp, string_codigo, NULL, maior_codigo);
        
	fprintf(fp, "DESCRICAO");
        char *string_descricao = (char*)malloc(10*sizeof(char));
	strcpy(string_descricao, "DESCRICAO");
        define_espacamento(fp, string_descricao, NULL, maior_descricao);

        fprintf(fp, "QUANT");
        char *string_quant = (char*)malloc(6*sizeof(char));
	strcpy(string_quant, "QUANT");
        define_espacamento(fp, string_quant, NULL, maior_quantidade);

        fprintf(fp, "UN");
        char *string_un = (char*)malloc(3*sizeof(char));
	strcpy(string_un, "UN");
        define_espacamento(fp, string_un, NULL, 2);

        fprintf(fp, "VAL UNIT");
        char *string_val_unit = (char*)malloc(9*sizeof(char));
	strcpy(string_val_unit, "VAL UNIT");
        define_espacamento(fp, string_val_unit, NULL, maior_valor_unitario);

        fprintf(fp,"VAL TOTAL");
        char *string_val_tot = (char*)malloc(10*sizeof(char));
	strcpy(string_val_tot, "VAL TOTAL");
        define_espacamento(fp, string_val_tot, NULL, 5);

        fprintf(fp, "\n");

//Imprimindo os produtos
        aux_nodo = list->head;
        int i, j;
	while(1){
                if(aux_nodo == NULL) break;

		produto_encontrado = busca_codigo(arvore, aux_nodo->codigo);
		if(produto_encontrado == NULL){
			cout << "Codigo ";
			for(i = 0; i < 13; i++)
				cout << aux_nodo->codigo[i];
			cout << " nao existe no banco" << endl;
			return;
		}

		for(i = 0; i < 13; i++)
			fprintf(fp, "%d", aux_nodo->codigo[i]);
	        fprintf(fp, "      ");
               

		fprintf(fp, "%s", produto_encontrado->descricao);
                define_espacamento(fp, string_descricao, produto_encontrado->descricao, maior_descricao);


                fprintf(fp, "%d", aux_nodo->quantidade);
		char *aux_quantidade = (char*)calloc(maior_quantidade, sizeof(char));
		int rest_div, i = 0;
		int quantidade = aux_nodo->quantidade;
		while(1){
			if(quantidade == 0) break;
			rest_div = quantidade%10;
			aux_quantidade[i] = static_cast<char>(rest_div);
			quantidade = quantidade/10;
		}
                define_espacamento(fp, string_quant, aux_quantidade, maior_quantidade);
		free(aux_quantidade);


		fprintf(fp, "%s", produto_encontrado->tipo);
                define_espacamento(fp, string_un, produto_encontrado->tipo, 2);


		for(i = 0; i < (strlen(produto_encontrado->valor_unitario) - 1); i++)
			fprintf(fp, "%c", produto_encontrado->valor_unitario[i]);
                define_espacamento(fp, string_val_unit, produto_encontrado->valor_unitario, maior_valor_unitario);


		float val_total = atof(produto_encontrado->valor_unitario);		
		val_total = val_total * aux_nodo->quantidade;
		fprintf(fp, "%.2f", val_total);
		
		int quant_digitos = quantidade_de_digitos(static_cast<int>(val_total));
		quant_digitos += 3;
		if(quant_digitos > maior_valor_total)
			maior_valor_total = quant_digitos;

		fprintf(fp, "\n");

		//Alterando a quantidade no banco de dados
		remove_produto_trie(arvore, aux_nodo->codigo, aux_nodo->quantidade);

		aux_nodo = aux_nodo->prox;	
        }

//Definindo o espacamento para o rodape da nota fiscal
	int espacamento = 13 + 2 + (5*6);						//Digito + Tipo + Espacamentos padrao
	if(strlen("DESCRICAO") < maior_descricao)
		espacamento += maior_descricao;
	else
		espacamento += strlen("DESCRICAO");
	
	if(strlen("QUANT") < maior_quantidade)
		espacamento += maior_quantidade;
	else
		espacamento += strlen("QUANT");

	if(strlen("VAL UNIT") < maior_valor_unitario)
		espacamento += maior_valor_unitario;
	else
		espacamento += strlen("VAL UNIT");

	if(strlen("VAL TOTAL") < maior_valor_total)
		espacamento += maior_valor_total;
	else
		espacamento += strlen("VAL TOTAL");

	for(i = 0; i <= espacamento; i++)
		fprintf(fp, "-");

//Imprimindo valor a pagar
        fprintf(fp, "\nVALOR A PAGAR");
	for(i = 0; i < (espacamento - quantidade_de_digitos(static_cast<int>(valor_da_venda)) - strlen("VALOR A PAGAR") - 4); i++)
		fprintf(fp, " ");
        fprintf(fp,"R$%.2f\n", valor_da_venda);

//Imprimindo forma de pagamento
	if(escolha == 1){
		fprintf(fp, "Dinheiro");
		tam = strlen("Dinheiro");
	}
	else if(escolha == 2){
		fprintf(fp, "Cartao de Debito");
		tam = strlen("Cartao de Debito");
	}
	else{
		fprintf(fp, "Cartao de Credito");
		tam = strlen("Cartao de Credito");
	}

//Imprimindo valor pago
	for(i = 0; i < (espacamento - tam - quantidade_de_digitos(static_cast<int>(valor_pago)) - 4); i++)
		fprintf(fp, " ");
	fprintf(fp, "R$%.2f\n", valor_pago);

//Caso seja pagamento por dinheiro, imprime o troco
	if(escolha == 1){
		fprintf(fp, "TROCO:");
		for(i = 0; i < (espacamento - strlen("TROCO:") - quantidade_de_digitos(static_cast<int>(valor_pago)) - 2); i++)
			fprintf(fp, " ");
		fprintf(fp, "R$%.2f\n", valor_pago - valor_da_venda);
	}

	for(i = 0; i <= espacamento; i++)
		fprintf(fp, "#");	

	fprintf(fp, "\n\n");

	free(string_codigo);
	free(string_descricao);
	free(string_quant);
	free(string_un);
	free(string_val_unit);
	free(string_val_tot);
}

