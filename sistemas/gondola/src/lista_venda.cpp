#include <iostream>
#include <cstring>
#include <cstdlib>
#include "../include/lista_venda.hpp"

using namespace std;

nodo *cria_nodo(){
        nodo *aux_nodo = (nodo*)malloc(sizeof(nodo));
        aux_nodo->codigo = NULL;
        aux_nodo->quantidade = 0;
        return aux_nodo;
}

lista *cria_lista (){
        lista *aux_lista = (lista*)malloc(sizeof(lista));
        aux_lista->head = aux_lista->tail = NULL;
        return aux_lista;
}

int *transforma_codigo(int codigo){
        int *vetor_codigo = (int*)calloc(13,sizeof(int));
        int aux_codigo = codigo;
        int i = 12;

        while(1){
                if(codigo == 0) break;
                aux_codigo = codigo%10;
                vetor_codigo[i] = aux_codigo;
                codigo = codigo/10;
                i--;
        }

        for(; i > 1; i--)
                vetor_codigo[i] = 0;

        return vetor_codigo;
}

nodo *busca_nodo_anterior_ao_produto (lista *list, int *cod){
        nodo *aux_nodo = list->head;

        while(1){
                if(aux_nodo->prox == NULL) return NULL;
		
		int i = 0;
		while(1){
			if(aux_nodo->prox->codigo[i] != cod[i] || i > 12) break;
			i++;
		}

		if(i == 13) return aux_nodo;
                aux_nodo = aux_nodo->prox;
        }
}

nodo *busca_nodo(lista *list, int *cod){
    nodo *aux_nodo = list->head;

    while(1){
        if(aux_nodo == NULL) return NULL;

	int i = 0;
	while(1){
		if(aux_nodo->codigo[i] != cod[i] || i > 12) break;
		i++;
	}
	if(i == 13) return aux_nodo;
        aux_nodo = aux_nodo->prox;
    }
}

void insere_na_tail (lista *list, int cod){
	int *codigo_transformado = transforma_codigo(cod);
	nodo *aux_nodo = busca_nodo(list, codigo_transformado);
	
	if(aux_nodo != NULL){
		aux_nodo->quantidade++;
		free(codigo_transformado);
	}
	else{
        	aux_nodo = cria_nodo();
        	aux_nodo->codigo = codigo_transformado;
        	aux_nodo->quantidade++;
		aux_nodo->prox = NULL;
		if(list->head == NULL && list->tail == NULL)
            		list->head = list->tail = aux_nodo;
        	else{
            		list->tail->prox = aux_nodo;
            		list->tail = aux_nodo;
        	}
    	}
}

void remove_produto_lista(lista *list, int cod){
	int quantidade_retirar, verificacao;
	int *codigo_transformado = transforma_codigo(cod);

	nodo *nodo_encontrado = busca_nodo(list, codigo_transformado);
 	if(nodo_encontrado == NULL){
		imprime_comandos(8);
		return;
	}	
       
	cout << "   Quantidade a ser removida: ";
	cin >> quantidade_retirar;
	cout << "\n";

	if(nodo_encontrado->quantidade < quantidade_retirar){
        	imprime_comandos(9);
                return;
        }

	nodo_encontrado->quantidade = nodo_encontrado->quantidade - quantidade_retirar;
	if(nodo_encontrado->quantidade > 0){
       		cout << "   →       Retirou-se " << quantidade_retirar << " unidade(s) do produto       ←  \n";
                cout << "   →                    codigo " <<  cod <<  ".                  ←  " << endl;
		
		free(codigo_transformado);
		return;
	}

	nodo *aux_bloco;

	if(list->head == nodo_encontrado)
		list->head = nodo_encontrado->prox;
	else{
		aux_bloco = busca_nodo_anterior_ao_produto(list, codigo_transformado);
		aux_bloco->prox = nodo_encontrado->prox;
	}
	free(nodo_encontrado);
	imprime_comandos(10);
	
	if(aux_bloco->prox == NULL)
		list->tail = aux_bloco;

	free(codigo_transformado);
}

double calcula_total_da_venda(lista *list, trie *arvore){
	bloco *produto_encontrado;
	nodo *produto_na_lista = list->head;
	double valor_produto, valor_venda = 0;
	int i;

	while(1){
		if(produto_na_lista == NULL) break;
		
		i = 0;	
		produto_encontrado = busca_codigo(arvore, produto_na_lista->codigo);
		if(produto_encontrado == NULL){
			cout << "ERRO: Produto de codigo ";
			while(1){
				if(i == 13) break;
				cout << produto_na_lista->codigo[i++];
			}
			cout << " nao cadastrado." << endl;
			return 0;
		}
	
		valor_produto = stod(produto_encontrado->valor_unitario);
		valor_produto = valor_produto * produto_na_lista->quantidade;

		valor_venda += valor_produto;

		produto_na_lista = produto_na_lista->prox;
	}

	return valor_venda;
}

void libera_nodo(nodo *aux_nodo){
        if(aux_nodo == NULL)
                return;
        libera_nodo(aux_nodo->prox);
	free(aux_nodo->codigo);
        free(aux_nodo);
}


void libera_lista (lista *lista){
        libera_nodo(lista->head);
        free(lista);
}


