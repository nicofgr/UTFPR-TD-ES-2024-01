#include <iostream>
#include <cstring>
#include "../include/trie.hpp"
#define MAX 1000

using namespace std;

bloco *cria_bloco(){
	bloco *aux_bloco = (bloco*)calloc(1,sizeof(bloco));

	aux_bloco->marcacao = 0;
	aux_bloco->descricao = NULL; 
	aux_bloco->quantidade = NULL;
	aux_bloco->tipo = NULL;
	aux_bloco->valor_unitario = NULL;

	for(int i = 0; i < 10; i++)
		aux_bloco->caminho[i] = NULL;

	return aux_bloco;
}

trie *cria_arvore(){
	trie *aux_arvore = (trie*)calloc(1,sizeof(trie));
	
	aux_arvore->raiz = cria_bloco();
	
	return aux_arvore;
}

char *separa_conteudo(char *conteudo, int *posicao){
	char *aux_string = (char*)calloc(1,sizeof(char));
	int i = 0;

	while(1){
                if(conteudo[posicao[0]] == ','){
                        posicao[0]++;
			break;
                }
                else if(conteudo[posicao[0]] == '\n') break;
		aux_string = (char*)realloc(aux_string, (i + 1)*sizeof(char));
                aux_string[i] = conteudo[posicao[0]];
                posicao[0]++;
                i++;
	}

	return aux_string;
}

int *inverte_para_int(char *conteudo){
        int i = 0;
        int *aux_array = (int*)calloc((strlen(conteudo) - 1), sizeof(int));

        while(1){
                if(i == (strlen(conteudo) - 1)) break;
                aux_array[i] = conteudo[i] - '0';
                i++;
        }

        return aux_array;
}

char *inverte_para_char(int *conteudo){
        int i = 0;
        int tam;
        char *aux_array;

        if(conteudo[0] == 0)                                                            //Caso seja um codigo com um ou mais zeros em seu inicio (sizeof ignra os zeros, resultando em um erro no tamanho real do int*)
                tam = 13;
        else
                tam = sizeof(conteudo)/sizeof(conteudo[0]);

        aux_array = (char*)calloc(tam, sizeof(char));

        while(1){
                if(i == tam) break;
                aux_array[i] = conteudo[i] + '0';
                i++;
        }

        return aux_array;
}

void insere_trie(trie *arvore, char *conteudo){
	if(conteudo == NULL) return;		

	char *string_retornada;
	int *posicao = (int*)calloc(1,sizeof(int));	
							
	int i;
	
	posicao[0] = 0;

	bloco *aux_bloco = arvore->raiz;
	string_retornada = separa_conteudo(conteudo, posicao);
	i = 0;


	while(aux_bloco->caminho[string_retornada[i] - '0'] != NULL){
		aux_bloco = aux_bloco->caminho[string_retornada[i] - '0'];
		i++;
	}

	while(1){
        	if(i == strlen(string_retornada)) break;
	   	aux_bloco->caminho[string_retornada[i] - '0'] = cria_bloco();
        	aux_bloco = aux_bloco->caminho[string_retornada[i] - '0'];
		i++;
	}
    	free(string_retornada);

    	aux_bloco->marcacao = 1;
    	
	string_retornada = separa_conteudo(conteudo, posicao); 
	aux_bloco->descricao = string_retornada;

	string_retornada = separa_conteudo(conteudo, posicao);
	aux_bloco->quantidade = string_retornada;

    	string_retornada = separa_conteudo(conteudo, posicao);
	aux_bloco->tipo = string_retornada;

   	string_retornada = separa_conteudo(conteudo, posicao);
	aux_bloco->valor_unitario = string_retornada;

	free(posicao);
}

void le_arquivo(FILE *fp, trie *arvore, double *dinheiro){
	char conteudo[MAX];

	fp = fopen("tabela_modificada.txt", "r");
	if(fp == NULL)
		fp = fopen("tabela.txt", "r");

	if(fp == NULL){
		cout << "ERRO: arquivo nao encontrado" << endl;
		exit(1);
	}

	fscanf(fp, "%lf\n", dinheiro);

	while(1){
		if(fgets(conteudo, MAX, fp) == NULL) break;
		insere_trie(arvore, conteudo);
	}

	fclose(fp);
}


bloco *busca_codigo(trie *arvore, int *codigo){
	bloco *aux_raiz = arvore->raiz;
	int i = 0, quant_caminho = 0;	
	
	while(1){
		if(aux_raiz->caminho[codigo[i]] == NULL && aux_raiz->marcacao == 0) return NULL;	
		aux_raiz = aux_raiz->caminho[codigo[i]];
		if(aux_raiz->marcacao == 1 && i == 12) return aux_raiz;
		i++;
	}
}

int verifica_arvore_vazia(trie *arvore){
	int vazia = 1;

	for(int i = 0; i < 10; i++){
		if(arvore->raiz->caminho[i] != NULL){
			vazia = 0;
			break;
		}
	}

	if(vazia == 1) free(arvore->raiz);

	return vazia;

}

bloco *busca_qualquer_bloco(trie *arvore, int *passos){
	bloco *aux_raiz = arvore->raiz;
	int prox, i = 0;

	while(aux_raiz->marcacao != 1){
		prox = 0;
		while(1){
			//Encontrando um caminho possivel
			if(aux_raiz->caminho[prox] != NULL){
				aux_raiz = aux_raiz->caminho[prox];
				break;
			}
			prox++;
			if(prox == 10) break;
		}
		passos[i] = prox;
		i++;
	}

	return aux_raiz;
}


void libera_bloco(bloco *noh){
	if(noh->marcacao == 1){
		free(noh->descricao);
		free(noh->quantidade);
		free(noh->tipo);
		free(noh->valor_unitario);	
	}
	free(noh);
}


void libera_caminho(trie *arvore, int *codigo){
	int prox, i = 13, existe_outros_blocos = 0;
	bloco *aux_raiz;
	bloco *bloco_anterior;

	while(1){
		if(existe_outros_blocos == 1) break;
		if(i == 0) break;
		aux_raiz = arvore->raiz->caminho[codigo[0]];
		prox = 0;

		while(1){
			prox++;
			if(prox == i){
				for(int j = 0; j < 10; j++){
					if(aux_raiz->caminho[j] != NULL){
						existe_outros_blocos = 1;
						break;
					}
				}
				if(existe_outros_blocos == 1) break;
				libera_bloco(aux_raiz);
				bloco_anterior->caminho[codigo[prox - 1]] = NULL;
				break;
			}

			if(existe_outros_blocos == 1) break;	
			
			bloco_anterior = aux_raiz;	
			aux_raiz = aux_raiz->caminho[codigo[prox]];
		}
		i--;
	}

	if(existe_outros_blocos == 0)
		arvore->raiz->caminho[codigo[0]] = NULL;

}


void salva_trie(FILE *fp, trie *arvore, double *dinheiro_mercado){
	bloco *buscado;
	bloco *aux_raiz;
	int *blocos_passados = (int*)calloc(13, sizeof(int));
	int condicao_vazia;

	fprintf(fp, "%.2f\n", dinheiro_mercado[0]); 
	
	while(1){
		condicao_vazia = verifica_arvore_vazia(arvore);
		if(condicao_vazia == 1) return;

		buscado = busca_qualquer_bloco(arvore, blocos_passados);
		if(buscado == NULL) break;

		for(int i = 0; i < 13; i++)
			fprintf(fp, "%d", blocos_passados[i]);
		fprintf(fp,",%s,", buscado->descricao);
		
		if(strcmp(buscado->tipo, "KG") == 0)
		 	fprintf(fp,"%.3f", stod(buscado->quantidade));	
		else
			fprintf(fp,"%d", stoi(buscado->quantidade));

		fprintf(fp, ",%s,%s\n", buscado->tipo, buscado->valor_unitario);
		
		libera_caminho(arvore, blocos_passados);
	}
}


void remove_produto_trie(trie *arvore, int *codigo, double quantidade_retirar){
	bloco *buscado = busca_codigo(arvore, codigo);
	if(buscado == NULL){
		cout << "ERRO: Produto nao existe no banco de dados" << endl;
		return;
	}

	double quantidade = stod(buscado->quantidade);
	if(quantidade < quantidade_retirar){
		cout << "\n\n   ERRO: Existe menos produtos do que deseja-se remover." << endl;
		exit(1);
	}

	quantidade = quantidade - quantidade_retirar;

	if(quantidade > 0)
		strcpy(buscado->quantidade,to_string(quantidade).data());
	else
		libera_caminho(arvore, codigo);	
}

void libera_trie(bloco *raiz){

	int i = 0;
	while(1){
		if(raiz->caminho[i] == NULL && i < 10)
			i++;
		else if(i == 10){
			libera_bloco(raiz);
			break;
		}
		else{
			libera_trie(raiz->caminho[i]);
			raiz->caminho[i] = NULL;
		}
	}
}

