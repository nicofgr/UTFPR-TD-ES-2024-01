#ifndef TRIE_H_INCLUDED
#define TRIE_H_INCLUDED

typedef struct Bloco{
	struct Bloco *caminho[10];
	int marcacao;
	char *descricao, *quantidade, *tipo, *valor_unitario;
}bloco;

typedef struct Trie{
	bloco *raiz;
}trie;

bloco *cria_bloco();

trie *cria_arvore();

char *separa_conteudo(char *conteudo, int *posicao);

int *inverte_para_int(char *conteudo);

char *inverte_para_char(int *conteudo);

void insere_trie(trie *arvore, char *conteudo);

void le_arquivo(FILE *fp, trie *arvore, double *dinheiro);

bloco *busca_codigo(trie *arvore, int *codigo);

bloco *busca_qualquer_bloco(trie *arvore, int *passos);

void salva_trie(FILE *fp, trie *arvore, double *dinheiro_mercado);

void libera_bloco(bloco *bloco);

void libera_caminho(trie *arvore, int *codigo);

void remove_produto_trie(trie *arvore, int *codigo, double quantidade_retirar);

void libera_trie(bloco *raiz);

#endif
