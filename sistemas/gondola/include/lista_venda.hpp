#ifndef LISTA_VENDA_H_INCLUDED
#define LISTA_VENDA_H_INCLUDED
#include "trie.hpp"
#include "informacoes_terminal.hpp"

typedef struct Nodo{
        int *codigo, quantidade;
        struct Nodo *prox;
}nodo;

typedef struct Lista{
        nodo *head, *tail;
}lista;

nodo *cria_nodo();

lista *cria_lista();

int *transforma_codigo(int codigo);

nodo *busca_nodo_anterior_ao_produto (lista *list, int *cod);

nodo *busca_nodo(lista *list, int *cod);

void insere_na_tail (lista *list, int cod);

void remove_produto_lista(lista *list, int cod);

double calcula_total_da_venda(lista *list, trie *arvore);

void libera_nodo (nodo *bloco);

void libera_lista (lista *lista);

#endif


