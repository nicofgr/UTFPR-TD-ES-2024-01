#ifndef NOTA_FISCAL_H_INCLUDED
#define NOTA_VENDA_H_INCLUDED
#include "lista_venda.hpp"
#include "trie.hpp"

int quantidade_de_digitos(int codigo);

void define_espacamento (FILE *fp, char *coluna, char *atual, int maior);

void imprime_no_arquivo (FILE *fp, lista *list, trie *arvore, int escolha, float valor_da_venda, float valor_pago);

#endif
