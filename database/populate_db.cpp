#include<iostream>
#include<postgresql/libpq-fe.h>
#include<string>
#include<vector>

using namespace std;

string random_name();

string random_barcode(){
    string code = "789";
    while (code.size()<12){
        int randint = rand() %10;
        code = code + to_string(randint);
    }
    while (code.size()>13){
        code.pop_back();
    }
    int sum_even = 0;
    int sum_odd = 0;
    for(int i = 1; i <= 12; i = i+2){
        sum_even = sum_even + (code[i] - '0'); 
        sum_odd = sum_odd + (code[i-1] - '0');
    }
    int sum = sum_even*3 + sum_odd;
    int remainder = sum % 10;
    int check = 10 - remainder;
    if(check == 10) check = 0;
    code += to_string(check);
    return code;
}

string generate_random_product(){
    string nome = random_name();
    string barcode = random_barcode();
    int quant_estoque = (rand() %1001);
    int quant_gondola = rand() % 21;
    int preco_compra = rand() % 21;
    int preco_venda = preco_compra + (rand() %50) + 5; 
    int quant_min_estoque = (rand() %101) + 10;
    int quant_min_gondola = (rand() %15) + 5;
    int quant_em_espera = rand() %1001;
    string product_info = nome + ",'" + barcode + "'," + to_string(quant_estoque) + "," + to_string(quant_gondola) + "," + to_string(preco_compra) + "," + to_string(preco_venda) + "," + to_string(quant_min_estoque) +","+  to_string(quant_min_gondola) +","+ to_string(quant_em_espera);
    string full_command = "INSERT INTO produtos(nome, codigo_de_barra, quant_estoque, quant_gondola, preco_compra, preco_venda, quant_min_estoque, quant_min_gondola, quant_em_espera) VALUES (" + product_info + ")";
    return full_command;
}

int main(){
    srand(time(NULL));
    PGconn *conn;
    conn = PQconnectdb("dbname=mercado host=localhost user=mercado password=mercado");
    if(PQstatus(conn) == CONNECTION_BAD){
        puts("Nao foi possivel conectar ao banco de dados");
        exit(0);
    }

    for(int i = 0; i < 30; i++){
        string prod = generate_random_product();
        //cout << "COMMAND >>> " << prod << endl;
        PQexec(conn, prod.c_str());
    }
    return 0;
}

string random_name(){
    vector<string> products({
                            "Batata", "Agua", "Sabao em Po", "Vinho A", "Vinho B", "Vinho C", "Detergente", "Agua sanitaria",
                            "Cenoura", "Arroz", "Pera", "Carne de frango", "Racao", "Farinha", "Farinha de Trigo", "Esponja",
                            "Repolho", "Feijao", "Feijao Vegano", "Pao integral", "Pao Tradicional", "Carne de boi", "Vassoura",
                            "Mel", "Biscoito de polvilho", "Bolacha", "Refrigerante", "Pipoca", "Milho", "Cadeira de praia", 
                            "Leite condensado", "Fermento", "Doce de leite", "Creme de leite", "Vinagre", "Manteiga", "Margarina",
                            "Papel Higienico", "Garrafa", "Lampada", "Peixe", "Chocolate", "Cogumelos"});

    vector<string> sufix({ "Stock", "Asquife", "Vokson", "Spinkex", "Avu", "Klox", "Sublack", "Smeda", "Llale", "Alpite", "Yewfruit",
                         "Bluecom", "Berryshadow", "Ecliprises", "Liongold", "Appiata", "Dynamico", "vegano", "Prime", "Cliff", "Lorien",
                         "Moria", "Gondor", "Bombadil", "Maggot", "Megalodo", "Comic-Sans", "Petunias", "Amarth", "da NASA", "da UTFPR",
                         "divino", "2 - Electric Boogaloo", "Miskatonic", "5a Edicao", "para jovens" });

    
    string full = "\'"+ products[rand() %(products.size() )] + " " + sufix[rand() %(sufix.size() )] + "\'";
    return full;
}
