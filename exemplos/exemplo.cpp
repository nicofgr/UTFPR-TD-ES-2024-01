#include <iostream>
#include <postgresql/libpq-fe.h>  // Biblioteca para acessar o banco de dados

using namespace std;

void printMenu();

int main(){

    PGconn *conn;  // Conexao com a db

    //postgresql://[user[:password]@][netloc][:port][/dbname][?param1=value1&...]
    //conn = PQconnectdb("postgresql://employee:senha@localhost:54322/postgres");  // Estabelecendo a conexao LOCAL com a db
    conn = PQconnectdb("postgres://employee.lcbbxrcxfadaaqdvoyzx:senha@aws-0-sa-east-1.pooler.supabase.com:6543/postgres");  // Estabelecendo a conexao LOCAL com a db


    if (PQstatus(conn) == CONNECTION_BAD) {  // Testando conexao
        cout << "Nao foi possivel estabelecer conexao com o banco de dados." << endl;
        exit(0);
    }

    //--------------------------------------------------------------

    PGresult *res;
    //res = PQexec(conn, "SELECT get_valor_total_pedido(1);");  // Mandando querry para db
    //res = PQexec(conn, "SELECT nome FROM produto ORDER BY length(nome)");  // Mandando querry para db
    res = PQexec(conn, "call registrar_produto('bananaaasdsada', '0000000000555');");  // Mandando querry para db


    if (PQresultStatus(res) != PGRES_TUPLES_OK) {
        cout << "Nenhum dado recebido." << endl;
        exit(0);
    }
 

    int rec_count = PQntuples(res);
    cout << rec_count << " dados recebidos." << endl;


    cout << "==========================" << endl;
    for (int row=0; row<rec_count; row++) {
        for (int col=0; col<2; col++) {
            cout << PQgetvalue(res, row, col) << "     ";  // lendo valores da querry
        }
    cout << endl;
    }
    cout << "==========================" << endl;

    //--------------------------------------------------------------
    res = PQexec(conn, "INSERT INTO financeiro.produtos (nome, codigo_de_barra, preco_compra, quant_em_espera) \
                        VALUES('BANANA', '0000001000000', 100000, 1)");  // Adicionando produto na tabela 
    PQfinish(conn);

    conn = PQconnectdb("dbname=mercado host=localhost user=sup_vendas password=senha");  // Estabelecendo a conexao com a db
    if (PQstatus(conn) == CONNECTION_BAD) {  // Testando conexao
        cout << "Nao foi possivel estabelecer conexao com o banco de dados." << endl;
        exit(0);
    }
    res = PQexec(conn, "UPDATE financeiro.produtos SET (preco_venda, quant_min_estoque, quant_min_gondola) = \
                        (1000000, 300, 20) WHERE codigo_de_barra = '0000001000000'");  // Adicionando valores para o produto adicionado

    PQclear(res);
    PQfinish(conn);
    return 0; 
}

