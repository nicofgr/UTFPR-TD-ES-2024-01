# UTFPR-TD-ES-2024-01

Projeto de fábrica de software da unidade curricular de Engenharia de Software 2024-01 do curso de Engenharia da Computação da UTFPR Toledo.

## Pré-requisitos:

### libpq
Biblioteca para acessar o banco de dados pelo C++.

    $ sudo apt-get install libpq-dev

### C++ 17 ou superior

## Estrutura do banco de dados:

![Estrutura Banco](./images/bancobanco.png "Estrutura Banco")